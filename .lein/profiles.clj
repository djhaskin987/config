{
 :user
 {
  :dependencies [[pjstadig/humane-test-output "0.8.1"]
                 [lein-ancient "0.6.10"]
                 [clj-stacktrace "0.2.8"]]

  :injections [(let [orig (ns-resolve (doto 'clojure.stacktrace require)
                                      'print-cause-trace)
                     new (ns-resolve (doto 'clj-stacktrace.repl require)
                                     'pst)]
                 (alter-var-root orig (constantly (deref new))))
               (require 'pjstadig.humane-test-output)
               (pjstadig.humane-test-output/activate!)]
  :plugins [[lein-cloverage "1.0.6"]
            [lein-try "0.4.3"]]
  }
 }
