# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022
PROFILE_INCLUDED=1

export LANG="en_US.UTF-8"
export LOCALE="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export LANGUAGE="en_US:"

export TERMCAP="${HOME}/.config/termcap"
export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin:/usr/games"

for i in /etc/profile.d/*.sh
do
    . ${i}
done

define_dir() {
    name="${1}"
    shift
    value="${1}"
    shift
    export ${name}="${value}"
    mkdir -p "${value}"
}

define_dir home_prefix "${HOME}/.local"
define_dir home_exec_prefix "${home_prefix}"
define_dir home_bindir "${home_prefix}/bin"
# We're in the home directory.
define_dir home_sbindir "${home_prefix}/bin"
export home_lib="lib"
define_dir home_libdir "${home_prefix}/${home_lib}"
define_dir home_libexecdir "${home_prefix}/libexec"
define_dir home_datarootdir "${home_prefix}/share"

define_dir home_sysconfdir "${HOME}/.config"
define_dir home_sharedstatedir "${HOME}/.com"
define_dir home_localstatedir "${HOME}/.appdata"
define_dir home_runstatedir "${home_localstatedir}/run"
define_dir home_logdir "${home_localstatedir}/log"
chmod 0700 "${home_runstatedir}"
define_dir home_rundatadir "${home_localstatedir}/lib"
define_dir home_cachedir "${home_localstatedir}/cache"

if [ -z "${LD_LIBRARY_PATH}" ]
then
    LD_LIBRARY_PATH="/usr/local/lib64:/usr/local/lib:/usr/lib64:/usr/lib"
fi
export LD_LIBRARY_PATH="${home_libdir}:${LD_LIBRARY_PATH}"

# The following XDG variables are set according to the standard at
# http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
export XDG_DATA_HOME="${home_datarootdir}"
export XDG_CONFIG_HOME="${home_sysconfdir}"
export XDG_CACHE_HOME="${home_cachedir}"
export XDG_RUNTIME_DIR="${home_runstatedir}"
export XDG_DATA_DIRS="${XDG_DATA_HOME}:/usr/local/share:/usr/share"
export XDG_CONFIG_DIRS="${XDG_CONFIG_HOME}:/etc/xdg"
export XDG_CONFIG_HOME="${home_sysconfdir}"
export XDG_DATA_DIRS="${home_datarootdir}:/usr/local/share/:/usr/share/"


define_dir home_includedir "${home_prefix}/include"
# Think "opt"
define_dir home_altprefix "${HOME}/.apps"

for possible_gpg_run_dir in "/run/user/$(id -u)/gnupg" "/home/$(whoami)/.gnupg"
do
    sock_loc="${possible_gpg_run_dir}/S.gpg-agent.ssh"
    if [ -e "${sock_loc}" ]
    then
        export "SSH_AUTH_SOCK=${sock_loc}"
        break
    fi
done

for i in $(ls -1A ${HOME}/.gem/ruby)
do
    export PATH="${HOME}/.gem/ruby/${i}/bin:${PATH}"
done
export PATH="${home_bindir}:${HOME}/bin:${PATH}"
export JAVA_HOME="${HOME}/.apps/jdk1.8.0_131"

if [ -n "${JAVA_HOME}" ]
then
    export PATH="${JAVA_HOME}/bin:${PATH}"
fi

if [ -n "${GEM_HOME}" ]
then
    export PATH="${GEM_HOME}/bin:${PATH}"
fi

for i in $(ls -1A ${home_altprefix})
do
    if [ -d "${home_altprefix}/${i}" -a -d "${home_altprefix}/${i}/bin" ]
    then
        export PATH="${home_altprefix}/${i}/bin:${PATH}"
    fi
done
export GOPATH="${HOME}/Development"
export GOROOT="/usr/local/go"
export PATH="${GOROOT}/bin:${GOPATH}/bin:${PATH}"
export PYTHONPATH="${HOME}/.python:${home_libdir}/python:/usr/lib/python:/usr/local/lib/python:${PYTHONPATH}"
export SAGE_PATH="${HOME}/.sage:${home_libdir}/sage:/usr/lib/sage:/usr/local/lib/sage:${SAGE_PATH}"
# set editor
if which djh987-emacs >/dev/null 2>&1
then
   export EDITOR=djh987-emacs
   export GUI_EDITOR=djh987-emacs
elif which emacsclient >/dev/null 2>&1
then
   export EDITOR=emacsclient
   export GUI_EDITOR=emacsclient
elif which vim >/dev/null 2>&1
then
    export EDITOR=vim
    export GUI_EDITOR=gvim
else
    # You've got problems
    export EDITOR=vi
fi

export BROWSER="google-chrome"
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
export HISTCONTROL=ignoredups:ignorespace
export HISTSIZE=1000
export HISTFILESIZE=2000

#export TERM=xterm-256color

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -z "${BASHRC_INCLUDED}" -a -s "${HOME}/.bashrc" ]
    then
        . ${HOME}/.bashrc
    fi
fi
